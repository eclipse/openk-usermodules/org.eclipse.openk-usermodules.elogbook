/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.common.mapper;

import com.google.gson.reflect.TypeToken;
import org.eclipse.openk.elogbook.common.Globals;
import org.eclipse.openk.elogbook.common.JsonGeneratorBase;
import org.eclipse.openk.elogbook.common.util.ResourceLoaderBase;
import org.eclipse.openk.elogbook.exceptions.BtbGone;
import org.eclipse.openk.elogbook.persistence.model.RefBranch;
import org.eclipse.openk.elogbook.persistence.model.RefGridTerritory;
import org.eclipse.openk.elogbook.persistence.model.TblResponsibility;
import org.eclipse.openk.elogbook.viewmodel.Responsibility;
import org.eclipse.openk.elogbook.viewmodel.TerritoryResponsibility;
import org.junit.Before;
import org.junit.Test;
import org.powermock.reflect.Whitebox;

import java.util.ArrayList;
import java.util.List;

import static org.eclipse.openk.elogbook.common.mapper.ResponsibilityMapper.PROCESS_MODE.CONFIRM;
import static org.eclipse.openk.elogbook.common.mapper.ResponsibilityMapper.PROCESS_MODE.PLAN;
import static org.junit.Assert.*;

public class ResponsibilityMapperTest extends ResourceLoaderBase {

  private List<TblResponsibility> tblResponsibilityList = new ArrayList<>();

  @Before
  public void createResponsibilityList() {

    RefBranch refBranchElectricity = new RefBranch();
    refBranchElectricity.setName(Globals.ELECTRICITY_MARK);
    RefBranch refBranchGas = new RefBranch();
    refBranchGas.setName(Globals.GAS_MARK);
    RefBranch refBranchDistrictHeat = new RefBranch();
    refBranchDistrictHeat.setName(Globals.DISTRICT_HEAT_MARK);
    RefBranch refBranchWater = new RefBranch();
    refBranchWater.setName(Globals.WATER_MARK);

    RefGridTerritory refGridTerritory1 = new RefGridTerritory();
    RefGridTerritory refGridTerritory2 = new RefGridTerritory();
    RefGridTerritory refGridTerritory3 = new RefGridTerritory();

    refGridTerritory1.setDescription("Mannheim");
    refGridTerritory2.setDescription("Offenbach");
    refGridTerritory3.setDescription("Stuttgart");

    //Mannheim all (4) responsibilities
    TblResponsibility tblResponsibility0 = new TblResponsibility();
    tblResponsibility0.setId(1);
    tblResponsibility0.setResponsibleUser("responsibleUser1");
    tblResponsibility0.setRefGridTerritory(refGridTerritory1);
    tblResponsibility0.setRefBranch(refBranchElectricity);

    TblResponsibility tblResponsibility1 = new TblResponsibility();
    tblResponsibility1.setId(2);
    tblResponsibility1.setResponsibleUser("responsibleUser1");
    tblResponsibility1.setRefGridTerritory(refGridTerritory1);
    tblResponsibility1.setRefBranch(refBranchGas);

    TblResponsibility tblResponsibility2 = new TblResponsibility();
    tblResponsibility2.setId(3);
    tblResponsibility2.setResponsibleUser("responsibleUser1");
    tblResponsibility2.setRefGridTerritory(refGridTerritory1);
    tblResponsibility2.setRefBranch(refBranchWater);

    TblResponsibility tblResponsibility3 = new TblResponsibility();
    tblResponsibility3.setId(4);
    tblResponsibility3.setResponsibleUser("responsibleUser1");
    tblResponsibility3.setNewResponsibleUser("newResponsibleUser1");
    tblResponsibility3.setRefGridTerritory(refGridTerritory1);
    tblResponsibility3.setRefBranch(refBranchDistrictHeat);

    //Offenbach 1 responsibility
    TblResponsibility tblResponsibility4 = new TblResponsibility();
    tblResponsibility4.setId(33);
    tblResponsibility4.setResponsibleUser("responsibleUser1");
    tblResponsibility4.setRefGridTerritory(refGridTerritory2);
    tblResponsibility4.setRefBranch(refBranchWater);

    //Stuttgart 2 responsibilities
    TblResponsibility tblResponsibility5 = new TblResponsibility();
    tblResponsibility5.setId(44);
    tblResponsibility5.setResponsibleUser("responsibleUser1");
    tblResponsibility5.setNewResponsibleUser("newResponsibleUser1");
    tblResponsibility5.setRefGridTerritory(refGridTerritory3);
    tblResponsibility5.setRefBranch(refBranchDistrictHeat);

    TblResponsibility tblResponsibility6 = new TblResponsibility();
    tblResponsibility6.setId(45);
    tblResponsibility6.setResponsibleUser("responsibleUser1");
    tblResponsibility6.setRefGridTerritory(refGridTerritory3);
    tblResponsibility6.setRefBranch(refBranchWater);

    tblResponsibilityList.add(tblResponsibility0);
    tblResponsibilityList.add(tblResponsibility1);
    tblResponsibilityList.add(tblResponsibility2);
    tblResponsibilityList.add(tblResponsibility3);
    tblResponsibilityList.add(tblResponsibility4);
    tblResponsibilityList.add(tblResponsibility5);
    tblResponsibilityList.add(tblResponsibility6);

  }

  @Test
  public void testMapToContainerVModelList() {

    ResponsibilityMapper responsibilityMappy = ResponsibilityTestHelper.createMapper();
    List<TerritoryResponsibility> territoryResponsibilityList = responsibilityMappy.mapToContainerVModelList(tblResponsibilityList);

    assertTrue(territoryResponsibilityList.size() == 3);
    assertEquals("Mannheim", territoryResponsibilityList.get(0).getGridTerritoryDescription());
    assertEquals("responsibleUser1", territoryResponsibilityList.get(0).getResponsibilityList().get(0).getResponsibleUser() );
    assertTrue(territoryResponsibilityList.get(0).getResponsibilityList().get(0).getId() == 1);
    assertTrue(territoryResponsibilityList.get(0).getResponsibilityList().size() == 4);
    assertEquals(Globals.ELECTRICITY_MARK, territoryResponsibilityList.get(0).getResponsibilityList().get(0).getBranchName());

    assertTrue(territoryResponsibilityList.get(0).getResponsibilityList().get(3).getId() == 4);
    assertEquals("newResponsibleUser1", territoryResponsibilityList.get(0).getResponsibilityList().get(3).getNewResponsibleUser());

    assertEquals("Offenbach", territoryResponsibilityList.get(1).getGridTerritoryDescription() );
    assertEquals("Stuttgart", territoryResponsibilityList.get(2).getGridTerritoryDescription());
    assertTrue(territoryResponsibilityList.get(2).getResponsibilityList().size() == 2);
    assertTrue(territoryResponsibilityList.get(2).getResponsibilityList().get(0).isActive());

  }

  @Test
  public void testMapToContainerVModelList_EmptyList() {

    List<TblResponsibility> tblResponsibilityList = new ArrayList<>();
    ResponsibilityMapper responsibilityMappy = ResponsibilityTestHelper.createMapper();
    List<TerritoryResponsibility> territoryResponsibilities = responsibilityMappy.mapToContainerVModelList(tblResponsibilityList);

    assertTrue(territoryResponsibilities.size() == 0);

  }

  @Test
  public void testMapFromVModelList_planResponsibilities() throws BtbGone {
    String json = super.loadStringFromResource("testTerritoryResponsibility.json");
    String moduser = "modUser";
    List<TerritoryResponsibility> territoryResponsiblityList = JsonGeneratorBase
        .getGson().fromJson(json, new TypeToken<List<TerritoryResponsibility>>(){}.getType());

    ResponsibilityMapper responsibilityMappy = ResponsibilityTestHelper.createMapper();

    List<TblResponsibility> responsibilityList = responsibilityMappy.
            mapFromVModelList(territoryResponsiblityList, tblResponsibilityList, PLAN, moduser);

    assertTrue(responsibilityList.size() == 7);
    assertTrue(responsibilityList.get(0).getId()== 1);
    assertTrue(responsibilityList.get(0).getResponsibleUser().equals("responsibleUser1"));
    assertTrue(responsibilityList.get(0).getNewResponsibleUser().equals("newResponsibleUser"));
    assertTrue(responsibilityList.get(0).getRefBranch().getName().equals(Globals.ELECTRICITY_MARK));

    assertTrue(responsibilityList.get(5).getId()== 44);
    assertTrue(responsibilityList.get(5).getResponsibleUser().equals("responsibleUser1"));
    assertTrue(responsibilityList.get(5).getNewResponsibleUser().equals("newResponsibleUser2"));
    assertTrue(responsibilityList.get(5).getRefBranch().getName().equals(Globals.DISTRICT_HEAT_MARK));
    assertNull(responsibilityList.get(6).getNewResponsibleUser());

  }


  @Test
  public void testMapFetchFromVModelList() throws BtbGone {
    String json = super.loadStringFromResource("testTerritoryResponsibility.json");
    String moduser = "modUser";
    List<TerritoryResponsibility> territoryResponsiblityList = JsonGeneratorBase
            .getGson().fromJson(json, new TypeToken<List<TerritoryResponsibility>>(){}.getType());

    ResponsibilityMapper responsibilityMappy = ResponsibilityTestHelper.createMapper();

    List<TblResponsibility> responsibilityList = responsibilityMappy.
            mapFetchFromVModelList(territoryResponsiblityList, tblResponsibilityList, moduser);
    // new User always differs from moduser => therefor the result is empty
    assertTrue( responsibilityList.isEmpty());

    // New user = moduser
    territoryResponsiblityList.get(0).getResponsibilityList().get(0).setNewResponsibleUser(moduser);
    territoryResponsiblityList.get(2).getResponsibilityList().get(0).setNewResponsibleUser(moduser);
    responsibilityList = responsibilityMappy.
            mapFetchFromVModelList(territoryResponsiblityList, tblResponsibilityList, moduser);
    assertEquals(2, responsibilityList.size());

    tblResponsibilityList.get(0).setResponsibleUser(moduser); // should eliminate 2 entries from the result list
    territoryResponsiblityList.get(0).getResponsibilityList().get(1).setNewResponsibleUser(moduser); // adds one the list
    responsibilityList = responsibilityMappy.
            mapFetchFromVModelList(territoryResponsiblityList, tblResponsibilityList, moduser);
    assertEquals( 1, responsibilityList.size());

    territoryResponsiblityList.get(0).getResponsibilityList().get(1).setNewResponsibleUser(moduser);
    tblResponsibilityList.get(1).setNewResponsibleUser("moduser");
    responsibilityList = responsibilityMappy.
            mapFetchFromVModelList(territoryResponsiblityList, tblResponsibilityList, moduser);
    assertEquals( 0, responsibilityList.size());
  }

  @Test( expected = BtbGone.class)
  public void testMapFromVModelListGridLocationException() throws BtbGone {
    String json = super.loadStringFromResource("testTerritoryResponsibilityBranchException.json");
    String moduser = "modUser";
    List<TerritoryResponsibility> territoryResponsibilities = JsonGeneratorBase
        .getGson().fromJson(json, new TypeToken<List<TerritoryResponsibility>>(){}.getType());
    ResponsibilityMapper responsibilityMappy = ResponsibilityTestHelper.createMapper();
    responsibilityMappy.mapFromVModelList(territoryResponsibilities, tblResponsibilityList, PLAN, moduser);

  }

  @Test( expected = BtbGone.class)
  public void testMapFromVModelListRespUserException() throws BtbGone {
    String json = super.loadStringFromResource("testTerritoryResponsibilityUserException.json");
    String moduser = "modUser";
    List<TerritoryResponsibility> territoryResponsiblityList = JsonGeneratorBase
        .getGson().fromJson(json, new TypeToken<List<TerritoryResponsibility>>(){}.getType());
    ResponsibilityMapper responsibilityMappy = ResponsibilityTestHelper.createMapper();
    responsibilityMappy.mapFromVModelList(territoryResponsiblityList, tblResponsibilityList, PLAN, moduser);

  }

  @Test( expected = BtbGone.class)
  public void testMapFromVModelListBranchException() throws BtbGone {
    String json = super.loadStringFromResource("testTerritoryResponsibilityBranchException.json");
    String moduser = "modUser";
    List<TerritoryResponsibility> territoryResponsiblityList = JsonGeneratorBase
        .getGson().fromJson(json, new TypeToken<List<TerritoryResponsibility>>(){}.getType());
    ResponsibilityMapper responsibilityMappy = ResponsibilityTestHelper.createMapper();
    responsibilityMappy.mapFromVModelList(territoryResponsiblityList, tblResponsibilityList, PLAN, moduser);

  }

  @Test( expected = BtbGone.class)
  public void testMapFromVModelListResponsibilityOutdatedPlus() throws BtbGone {
    String json = super.loadStringFromResource("testResponsibilityOutdatedDataPlus.json");
    String moduser = "modUser";
    List<TerritoryResponsibility> territoryResponsiblityList = JsonGeneratorBase
        .getGson().fromJson(json, new TypeToken<List<TerritoryResponsibility>>(){}.getType());
    ResponsibilityMapper responsibilityMappy = ResponsibilityTestHelper.createMapper();
   responsibilityMappy.mapFromVModelList(territoryResponsiblityList, tblResponsibilityList, PLAN, moduser);

  }

  @Test( expected = BtbGone.class)
  public void testMapFromVModelListResponsibilityOutdatedMinus() throws BtbGone {
    String json = super.loadStringFromResource("testResponsibilityOutdatedDataMinus.json");
    String moduser = "modUser";
    List<TerritoryResponsibility> territoryResponsiblityList = JsonGeneratorBase
        .getGson().fromJson(json, new TypeToken<List<TerritoryResponsibility>>(){}.getType());
    ResponsibilityMapper responsibilityMappy = ResponsibilityTestHelper.createMapper();
    responsibilityMappy.mapFromVModelList(territoryResponsiblityList, tblResponsibilityList, PLAN, moduser);
  }

  @Test
  public void testMapFromVModelList_confirmResponsibilities() throws BtbGone {
    String json = super.loadStringFromResource("testTerritoryResponsibility.json");
    String moduser = "modUser";
    List<TerritoryResponsibility> territoryResponsiblityList = JsonGeneratorBase.getGson()
        .fromJson(json, new TypeToken<List<TerritoryResponsibility>>() {
        }.getType());

    ResponsibilityMapper responsibilityMappy = ResponsibilityTestHelper.createMapper();

    List<TblResponsibility> responsibilityList = responsibilityMappy
        .mapFromVModelList(territoryResponsiblityList, tblResponsibilityList, CONFIRM, moduser);

    assertTrue(responsibilityList.size() == 7);
    assertTrue(responsibilityList.get(0).getId() == 1);
    assertTrue(responsibilityList.get(0).getResponsibleUser().equals("newResponsibleUser"));
    assertNull(responsibilityList.get(0).getNewResponsibleUser());
    assertTrue(responsibilityList.get(0).getRefBranch().getName().equals(Globals.ELECTRICITY_MARK));

    assertTrue(responsibilityList.get(5).getId() == 44);
    assertTrue(responsibilityList.get(5).getResponsibleUser().equals("responsibleUser1"));
    assertNull(responsibilityList.get(5).getNewResponsibleUser());
    assertTrue(responsibilityList.get(5).getRefBranch().getName().equals(Globals.DISTRICT_HEAT_MARK));

  }

  @Test( expected = BtbGone.class)
  public void testMapFromVModelListGridLocationException1() throws BtbGone {
    String json = super.loadStringFromResource("testTerritoryResponsibilityBranchException.json");
    String moduser = "modUser";
    List<TerritoryResponsibility> territoryResponsibilities = JsonGeneratorBase
            .getGson().fromJson(json, new TypeToken<List<TerritoryResponsibility>>(){}.getType());
    ResponsibilityMapper responsibilityMappy = ResponsibilityTestHelper.createMapper();
    responsibilityMappy.mapFromVModelList(territoryResponsibilities, tblResponsibilityList, CONFIRM, moduser);
  }
  @Test( expected = BtbGone.class)
  public void testMapFromVModelListRespUserException1() throws BtbGone {
    String json = super.loadStringFromResource("testTerritoryResponsibilityUserException.json");
    String moduser = "modUser";
    List<TerritoryResponsibility> territoryResponsiblityList = JsonGeneratorBase
            .getGson().fromJson(json, new TypeToken<List<TerritoryResponsibility>>(){}.getType());
    ResponsibilityMapper responsibilityMappy = ResponsibilityTestHelper.createMapper();
    responsibilityMappy.mapFromVModelList(territoryResponsiblityList, tblResponsibilityList, CONFIRM, moduser);
  }
  @Test( expected = BtbGone.class)
  public void testMapFromVModelListBranchException1() throws BtbGone {
    String json = super.loadStringFromResource("testTerritoryResponsibilityBranchException.json");
    String moduser = "modUser";
    List<TerritoryResponsibility> territoryResponsiblityList = JsonGeneratorBase
            .getGson().fromJson(json, new TypeToken<List<TerritoryResponsibility>>(){}.getType());
    ResponsibilityMapper responsibilityMappy = ResponsibilityTestHelper.createMapper();
    responsibilityMappy.mapFromVModelList(territoryResponsiblityList, tblResponsibilityList, CONFIRM, moduser);
  }
  @Test( expected = BtbGone.class)
  public void testMapFromVModelListResponsibilityOutdatedPlus1() throws BtbGone {
    String json = super.loadStringFromResource("testResponsibilityOutdatedDataPlus.json");
    String moduser = "modUser";
    List<TerritoryResponsibility> territoryResponsiblityList = JsonGeneratorBase
            .getGson().fromJson(json, new TypeToken<List<TerritoryResponsibility>>(){}.getType());
    ResponsibilityMapper responsibilityMappy = ResponsibilityTestHelper.createMapper();
    responsibilityMappy.mapFromVModelList(territoryResponsiblityList, tblResponsibilityList, CONFIRM, moduser);
  }
  @Test( expected = BtbGone.class)
  public void testMapFromVModelListResponsibilityOutdatedMinus1() throws BtbGone {
    String json = super.loadStringFromResource("testResponsibilityOutdatedDataMinus.json");
    String moduser = "modUser";
    List<TerritoryResponsibility> territoryResponsiblityList = JsonGeneratorBase
            .getGson().fromJson(json, new TypeToken<List<TerritoryResponsibility>>(){}.getType());
    ResponsibilityMapper responsibilityMappy = ResponsibilityTestHelper.createMapper();
    responsibilityMappy.mapFromVModelList(territoryResponsiblityList, tblResponsibilityList, CONFIRM, moduser);
  }

  @Test
  public void testIsResponsibilityModifiedDuringFetch() throws Exception {
    ResponsibilityMapper mapper = ResponsibilityTestHelper.createMapper();
    Responsibility viewModelResp = new Responsibility();

    viewModelResp.setResponsibleUser("Bruno");
    viewModelResp.setNewResponsibleUser("Claudio");
    String modUser = "Claudio";

    assertFalse(Whitebox.invokeMethod(mapper, "isResponsibilityModifiedDuringFetch",
            viewModelResp, null, modUser ));

    assertTrue(Whitebox.invokeMethod(mapper, "isResponsibilityModifiedDuringFetch",
            viewModelResp, tblResponsibilityList.get(0), modUser ));

    assertFalse(Whitebox.invokeMethod(mapper, "isResponsibilityModifiedDuringFetch",
            viewModelResp, tblResponsibilityList.get(0), "Bretzelmann" ));

    viewModelResp.setNewResponsibleUser("");
    assertFalse(Whitebox.invokeMethod(mapper, "isResponsibilityModifiedDuringFetch",
            viewModelResp, tblResponsibilityList.get(0), modUser )); // False: New user <> mod User

    tblResponsibilityList.get(0).setNewResponsibleUser("Claudio");
    viewModelResp.setNewResponsibleUser(null);
    assertTrue(Whitebox.invokeMethod(mapper, "isResponsibilityModifiedDuringFetch",
            viewModelResp, tblResponsibilityList.get(0), modUser )); // True: Existing new user set to null

    tblResponsibilityList.get(0).setNewResponsibleUser("Claudio");
    viewModelResp.setNewResponsibleUser("");
    assertTrue(Whitebox.invokeMethod(mapper, "isResponsibilityModifiedDuringFetch",
            viewModelResp, tblResponsibilityList.get(0), modUser ));

    tblResponsibilityList.get(0).setNewResponsibleUser("Claudio");
    viewModelResp.setNewResponsibleUser("Bretzelmann");
    assertFalse(Whitebox.invokeMethod(mapper, "isResponsibilityModifiedDuringFetch",
            viewModelResp, tblResponsibilityList.get(0), modUser ));

    tblResponsibilityList.get(0).setNewResponsibleUser("Claudio");
    viewModelResp.setNewResponsibleUser("claudio");
    assertFalse(Whitebox.invokeMethod(mapper, "isResponsibilityModifiedDuringFetch",
            viewModelResp, tblResponsibilityList.get(0), modUser ));

    tblResponsibilityList.get(0).setNewResponsibleUser(null);
    viewModelResp.setNewResponsibleUser("");
    assertFalse(Whitebox.invokeMethod(mapper, "isResponsibilityModifiedDuringFetch",
            viewModelResp, tblResponsibilityList.get(0), modUser ));


    viewModelResp.setResponsibleUser("Claudio");
    tblResponsibilityList.get(0).setNewResponsibleUser("Claudio");
    assertTrue(Whitebox.invokeMethod(mapper, "isResponsibilityModifiedDuringFetch",
            viewModelResp, tblResponsibilityList.get(0), modUser ));
  }
}
