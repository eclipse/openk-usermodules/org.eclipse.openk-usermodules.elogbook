/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.common.mapper;

import org.eclipse.openk.elogbook.auth2.model.KeyCloakUser;
import org.eclipse.openk.elogbook.auth2.model.KeyCloakUserAccess;
import org.eclipse.openk.elogbook.viewmodel.UserAuthentication;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class KeyCloakUserMapperTest {
	private KeyCloakUser keyCloakUser;
	private List<KeyCloakUser> keyCloakUsers;

	@Before
	public void init() {
		this.keyCloakUser = new KeyCloakUser();
		this.keyCloakUsers = new ArrayList<>();
		this.keyCloakUser.setAccess(new KeyCloakUserAccess());
		this.keyCloakUser.setCreatedTimestamp(new Date().getTime());
		this.keyCloakUser.setDisableableCredentialTypes(new ArrayList<String>());
		this.keyCloakUser.setEmailVerified(Boolean.FALSE.booleanValue());
		this.keyCloakUser.setEnabled(Boolean.FALSE.booleanValue());
		this.keyCloakUser.setId("4711");
		this.keyCloakUser.setFirstName("Heinz");
		this.keyCloakUser.setLastName("Goergens");
		this.keyCloakUser.setRealmRoles(new ArrayList<String>());
		this.keyCloakUser.setRequiredActions(new ArrayList<String>());
		this.keyCloakUser.setTotp(Boolean.TRUE.booleanValue());
		this.keyCloakUser.setUsername("goergh");

		keyCloakUsers.add(keyCloakUser);

		this.keyCloakUser = new KeyCloakUser();
		this.keyCloakUser.setAccess(new KeyCloakUserAccess());
		this.keyCloakUser.setCreatedTimestamp(new Date().getTime());
		this.keyCloakUser.setDisableableCredentialTypes(new ArrayList<String>());
		this.keyCloakUser.setEmailVerified(Boolean.TRUE.booleanValue());
		this.keyCloakUser.setEnabled(Boolean.TRUE.booleanValue());
		this.keyCloakUser.setId("4712");
		this.keyCloakUser.setFirstName(null);
		this.keyCloakUser.setLastName(null);
		this.keyCloakUser.setRealmRoles(null);
		this.keyCloakUser.setRequiredActions(null);
		this.keyCloakUser.setTotp(Boolean.FALSE.booleanValue());
		this.keyCloakUser.setUsername("");

		keyCloakUsers.add(keyCloakUser);

	}

	@Test
	public void testMapping() {

		List<UserAuthentication> userAuthentications = KeyCloakUserMapper.mapFromKeyCloakUserList(keyCloakUsers);
		assertEquals(userAuthentications.get(0).getId(), "4711");
		assertEquals(userAuthentications.get(0).getName(), "Heinz Goergens");

		assertEquals(userAuthentications.get(0).getUsername(), "goergh");

		assertEquals("", userAuthentications.get(1).getName());
	}
	
	@Test
	public void testMappingWithParameterNull() {
		List<UserAuthentication> userAuthentications = KeyCloakUserMapper.mapFromKeyCloakUserList(null);
		assertTrue(userAuthentications.isEmpty());
	}

}