/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.common.Communication;

import org.eclipse.openk.elogbook.communication.RestServiceWrapper;
import org.eclipse.openk.elogbook.exceptions.BtbException;
import org.junit.Before;
import org.junit.Test;

public class RestServiceWrapperTest {

    private RestServiceWrapper restServiceWrapper;
    private boolean useHttps = true;

    @Before
    public void init() {
        this.restServiceWrapper = new RestServiceWrapper("testURL", useHttps);
    }

    @Test(expected = BtbException.class)
    public void testPerformGetRequest() throws BtbException {
        restServiceWrapper.performGetRequest("testParam", "testToken");
    }

    @Test(expected = BtbException.class)
    public void testPerformPostRequest() throws Exception {
        restServiceWrapper.performPostRequest("testParam", "testToken", "testData");
    }

}
