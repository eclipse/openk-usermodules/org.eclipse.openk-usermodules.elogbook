/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.common;


import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BackendConfigTest {
    @Test
    public void testConfig() {
        BackendConfig bc = BackendConfig.getInstance();
        assertEquals( "http://localhost:8080/portal/rest/beservice", bc.getPortalBaseURL() );
        assertEquals( "C:/FilesToImport", bc.getImportFilesFolderPath()  );
        assertEquals( 9 , bc.getFileRowToRead());
        BackendConfig.setConfigFileName("backendConfigProduction.json");
        assertEquals( "backendConfigProduction.json", BackendConfig.getConfigFileName() );
        assertEquals( "elogbook-access", bc.getApplicationAccessRole());
    }
}
