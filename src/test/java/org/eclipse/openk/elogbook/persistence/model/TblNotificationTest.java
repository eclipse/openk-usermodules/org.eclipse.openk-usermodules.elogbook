/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.persistence.model;

import static org.junit.Assert.*;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import org.junit.Test;

public class TblNotificationTest {
    @Test
    public void TestGettersAndSetters() {
        Timestamp ts = Timestamp.valueOf(LocalDateTime.now());
        TblNotification not = new TblNotification();
        not.setId(1);
        assertEquals((long) not.getId(), 1);

        not.setCreateDate(ts);
        assertEquals(ts, not.getCreateDate());

        not.setCreateUser("creatore");
        assertEquals("creatore", not.getCreateUser());

        not.setExpectedFinishedDate(ts);
        assertEquals(ts, not.getExpectedFinishedDate());

        not.setFinishedDate(ts);
        assertEquals(ts, not.getFinishedDate());

        not.setFreeText("blabla");
        assertEquals("blabla", not.getFreeText());

        not.setFreeTextExtended("fte");
        assertEquals("fte", not.getFreeTextExtended());

        not.setIncidentId(22);
        assertEquals(22, (int) not.getIncidentId());

        not.setModDate(ts);
        assertEquals(ts, not.getModDate());

        not.setModUser("musr");
        assertEquals("musr", not.getModUser());

        not.setNotificationText("notare");
        assertEquals("notare", not.getNotificationText());

        not.setReminderDate(ts);
        assertEquals(ts, not.getReminderDate());

        not.setBeginDate(ts);
        assertEquals(ts, not.getBeginDate());

        not.setResponsibilityControlPoint("controllare");
        assertEquals("controllare", not.getResponsibilityControlPoint());

        not.setResponsibilityForwarding("fwd");
        assertEquals("fwd", not.getResponsibilityForwarding());

        not.setVersion(55);
        assertEquals(55, (int) not.getVersion());

        RefBranch rb = new RefBranch();
        rb.setDescription("brunch");
        not.setRefBranch(rb);
        assertEquals("brunch", not.getRefBranch().getDescription());

        RefNotificationStatus rns = new RefNotificationStatus();
        rns.setName("offen");
        not.setRefNotificationStatus(rns);
        assertEquals("offen", not.getRefNotificationStatus().getName());

        RefGridTerritory refGridTerritory = new RefGridTerritory();
        refGridTerritory.setName("Mannheim");
        not.setRefGridTerritory(refGridTerritory);
        assertEquals("Mannheim", not.getRefGridTerritory().getName());
        
        not.setAdminFlag(Boolean.TRUE);
        assertTrue(not.isAdminFlag());

    }

}
