/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.common.mapper;

import java.util.ArrayList;
import java.util.List;
import org.eclipse.openk.elogbook.auth2.model.KeyCloakUser;
import org.eclipse.openk.elogbook.viewmodel.UserAuthentication;

public class KeyCloakUserMapper {

  private KeyCloakUserMapper(){}

  public static List<UserAuthentication> mapFromKeyCloakUserList(List<KeyCloakUser> keyCloakUserList){
    List<UserAuthentication> userAuthenticationList = new ArrayList<>();
    if (keyCloakUserList==null) {
      return userAuthenticationList;
    }

    for (KeyCloakUser keyCloakUser : keyCloakUserList) {
      userAuthenticationList.add(mapFromKeyCloakUser(keyCloakUser));
    }
    return userAuthenticationList;
  }

  private static UserAuthentication mapFromKeyCloakUser(KeyCloakUser keyCloakUser){
    UserAuthentication userAuthentication = new UserAuthentication();
    userAuthentication.setId(keyCloakUser.getId());
    userAuthentication.setUsername(keyCloakUser.getUsername());
    String firstName = keyCloakUser.getFirstName() != null ? keyCloakUser.getFirstName() : "" ;
    String lastName = keyCloakUser.getLastName() != null ? keyCloakUser.getLastName() : "" ;
    String fullName = firstName + " " + lastName;
    userAuthentication.setName(fullName.trim());
    return userAuthentication;
  }
}
