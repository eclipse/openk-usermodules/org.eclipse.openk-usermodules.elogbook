/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.common.util;

import java.io.Serializable;
import java.util.Comparator;
import org.eclipse.openk.elogbook.persistence.model.TblResponsibility;

public class Comparators {

  private Comparators() {
    throw new IllegalStateException("Utility class (Comparator Container)");
  }

  public static class TblResponsibilityIdComparator implements Comparator<TblResponsibility>, Serializable{

    private static final long serialVersionUID = 1L;

    @Override
    public int compare(TblResponsibility o1, TblResponsibility o2) {
      return o1.getId().compareTo(o2.getId());
    }
  }

}
