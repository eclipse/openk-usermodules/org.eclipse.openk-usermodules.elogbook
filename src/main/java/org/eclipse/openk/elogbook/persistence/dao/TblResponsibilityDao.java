/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.persistence.dao;

import org.eclipse.openk.elogbook.persistence.model.TblResponsibility;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;


public class TblResponsibilityDao extends GenericDaoJpa<TblResponsibility, Integer> {
    private static final String FROM = " from ";
    public TblResponsibilityDao() {
        super();
    }

    public TblResponsibilityDao(EntityManager em) {
        super(em);
    }

    @SuppressWarnings("unchecked")
    public List<TblResponsibility> getResponsibilitiesForUser(String responsibleUser) {

        try {
            String selectString = FROM + TblResponsibility.class.getSimpleName() + " v where UPPER(v.responsibleUser) = UPPER(:responsibleUser)";
            Query q = getEM().createQuery(selectString);
            q.setParameter("responsibleUser", responsibleUser);
            return refreshModelCollection((List<TblResponsibility>) q.getResultList());
        } catch (Throwable t) { //NOSONAR
            LOGGER.error(t.getMessage());
            return new ArrayList<>();
        }
    }

    @SuppressWarnings("unchecked")
    public List<TblResponsibility> getPlannedResponsibilitiesForUser(String newResponsibleUser) {

        try {
            String selectString = FROM + TblResponsibility.class.getSimpleName() + " v where UPPER(v.newResponsibleUser) = UPPER(:newResponsibleUser)";
            Query q = getEM().createQuery(selectString);
            q.setParameter("newResponsibleUser", newResponsibleUser);
            return refreshModelCollection((List<TblResponsibility>) q.getResultList());
        } catch (Throwable t) { //NOSONAR
            LOGGER.error(t.getMessage());
            return new ArrayList<>();
        }
    }

    @SuppressWarnings("unchecked")
    public List<TblResponsibility> getAllResponsibilities() {

        try {
            String selectString = FROM + TblResponsibility.class.getSimpleName() + " v";
            Query q = getEM().createQuery(selectString);
            return refreshModelCollection((List<TblResponsibility>) q.getResultList());
        } catch (Throwable t) { //NOSONAR
            LOGGER.error(t.getMessage());
            return new ArrayList<>();
        }
    }

    @SuppressWarnings("unchecked")
    public List<TblResponsibility> getNewOrCurrentResponisbleUser(String modUser) {

        try {
            String selectString = FROM + TblResponsibility.class.getSimpleName() + " v where UPPER(v.responsibleUser) = UPPER(:modUser) or UPPER(v.newResponsibleUser) = UPPER(:modUser)";
            Query q = getEM().createQuery(selectString);
            q.setParameter("modUser", modUser);
            return refreshModelCollection((List<TblResponsibility>) q.getResultList());
        } catch (Throwable t) { //NOSONAR
            LOGGER.error(t.getMessage());
            return new ArrayList<>();
        }
    }

	/**
	 * Find the TblResponsibilites for the given list of Ids.
	 *
	 * @param responsibilityIds
	 *            A List of {@link Integer} objects representing the Ids of the
	 *            responsibilities
	 * @return the list of tblResponsibilities
	 */
	public List<TblResponsibility> findResponsibilitiesByIdList(List<Integer> responsibilityIds) {

		List<TblResponsibility> tblResponsibilities = new ArrayList<>();
		for (Integer responsibilityId : responsibilityIds) {
			TblResponsibility tblResponsibility = findById(TblResponsibility.class, responsibilityId);
			if (tblResponsibility != null) {
				tblResponsibilities.add(tblResponsibility);
			}
		}
		return tblResponsibilities;
	}

}

