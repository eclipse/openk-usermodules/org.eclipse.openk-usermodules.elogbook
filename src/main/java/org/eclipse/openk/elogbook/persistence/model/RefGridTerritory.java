/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.persistence.model;

import javax.persistence.*;
import java.io.Serializable;


/**
 * The persistent class for the ref_grid_territory database table.
 *
 */
@Entity
@Table(name="REF_GRID_TERRITORY")
@NamedQuery(name="RefGridTerritory.findAll", query="SELECT r FROM RefGridTerritory r")
public class RefGridTerritory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
	private Integer id;

	@ManyToOne
	@JoinColumn(name="fk_ref_master")
	private RefGridTerritory refMaster;

	@Column(name = "fk_ref_master", nullable = false, updatable = false, insertable = false)
	private Integer fkRefMaster;

	@Column(name = "description")
	private String description;

	@Column(name = "name")
	private String name;

	public RefGridTerritory() {
		//  default constructor
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public RefGridTerritory getRefMaster() {
		return refMaster;
	}

	public void setRefMaster(RefGridTerritory refMaster) {
		this.refMaster = refMaster;
	}

	public Integer getFkRefMaster() {
		return fkRefMaster;
	}

	public void setFkRefMaster(Integer fkRefMaster) {
		this.fkRefMaster = fkRefMaster;
	}


}