package org.eclipse.openk.elogbook.persistence.util;

import org.eclipse.persistence.internal.helper.DatabaseField;
import org.eclipse.persistence.mappings.DatabaseMapping;
import org.eclipse.persistence.mappings.OneToOneMapping;
import org.eclipse.persistence.sessions.Session;

import java.sql.Types;
import java.text.MessageFormat;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UUIDConverter implements org.eclipse.persistence.mappings.converters.Converter
{
    private static final Logger LOG = Logger.getLogger(UUIDConverter.class.getName());

    @Override
    public UUID convertObjectValueToDataValue(Object objectValue, Session session)
    {
        return (UUID) objectValue;
    }

    @Override
    public UUID convertDataValueToObjectValue(Object dataValue, Session session)
    {
        return (UUID) dataValue;
    }

    @Override
    public boolean isMutable()
    {
        return false;
    }

    @Override
    public void initialize(DatabaseMapping mapping, Session session)
    {
        DatabaseField field = mapping.getField();
        field.setSqlType(Types.OTHER);
        field.setTypeName("java.util.UUID");
        field.setColumnDefinition("UUID");

        // Find the bi-directional references other objects have to this field, and update them to be uuid too
        for (DatabaseMapping m : mapping.getDescriptor().getMappings())
        {
            if (m instanceof OneToOneMapping)
            {
                OneToOneMapping oneToOneMapping = (OneToOneMapping) m;

                DatabaseField relationshipField = oneToOneMapping.getSourceToTargetKeyFields().get(field);
                if (relationshipField != null)
                {
                    relationshipField.setSqlType(Types.OTHER);
                    relationshipField.setColumnDefinition("UUID");
                    relationshipField.setTypeName("java.util.UUID");
                    if (LOG.isLoggable(Level.FINE))
                        LOG.log(Level.FINE, MessageFormat.format("Setting foreign key field {0} to be a UUID", relationshipField));
                }
            }
        }
    }
}