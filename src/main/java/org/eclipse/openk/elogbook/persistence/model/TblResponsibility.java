/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.persistence.model;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;


/**
 * The persistent class for the tbl_responsibility database table.
 *
 */
@Entity
@Table(name="TBL_RESPONSIBILITY")
@NamedQuery(name="TblResponsibility.findAll", query="SELECT t FROM TblResponsibility t")
public class TblResponsibility implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_RESP_ID_SEQ")
	@SequenceGenerator(name = "TBL_RESP_ID_SEQ", sequenceName = "TBL_RESP_ID_SEQ", allocationSize = 1)
	@Column(name = "id", updatable = false)
	private Integer id;

	@Column(name="responsible_user")
	private String responsibleUser;

	@Column(name="new_responsible_user")
	private String newResponsibleUser;

	@Column(name="create_date")
	private Timestamp createDate;

	@Column(name="create_user")
	private String createUser;

	@Column(name="mod_date")
	private Timestamp modDate;

	@Column(name="mod_user")
	private String modUser;

	//bi-directional many-to-one association to RefGridTerritory
	@ManyToOne
	@JoinColumn(name="fk_ref_grid_territory")
	private RefGridTerritory refGridTerritory;

	//bi-directional many-to-one association to RefBranch
	@ManyToOne
	@JoinColumn(name="fk_ref_branch")
	private RefBranch refBranch;

	public TblResponsibility() {
		// empty Standard constructor
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getResponsibleUser() {
		return responsibleUser;
	}

	public void setResponsibleUser(String responsibleUser) {
		this.responsibleUser = responsibleUser;
	}

	public String getNewResponsibleUser() {
		return newResponsibleUser;
	}

	public void setNewResponsibleUser(String newResponsibleUser) {
		this.newResponsibleUser = newResponsibleUser;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Timestamp getModDate() {
		return modDate;
	}

	public void setModDate(Timestamp modDate) {
		this.modDate = modDate;
	}

	public String getModUser() {
		return modUser;
	}

	public void setModUser(String modUser) {
		this.modUser = modUser;
	}

	public RefGridTerritory getRefGridTerritory() {
		return refGridTerritory;
	}

	public void setRefGridTerritory(RefGridTerritory refGridTerritory) {
		this.refGridTerritory = refGridTerritory;
	}

	public RefBranch getRefBranch() {
		return refBranch;
	}

	public void setRefBranch(RefBranch refBranch) {
		this.refBranch = refBranch;
	}
}