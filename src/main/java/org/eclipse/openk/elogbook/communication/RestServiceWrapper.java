/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.communication;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.openk.elogbook.auth2.util.JwtHelper;
import org.eclipse.openk.elogbook.common.Globals;
import org.eclipse.openk.elogbook.exceptions.*;
import org.eclipse.openk.elogbook.viewmodel.ErrorReturn;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class RestServiceWrapper {
    private static final Logger LOGGER = LogManager.getLogger(RestServiceWrapper.class.getName());
    private String baseURL;
    private boolean useHttps;

    public RestServiceWrapper(String baseURL, boolean https) {
        this.baseURL = baseURL;
        this.useHttps = https;
    }

    public String performGetRequest(String restFunctionWithParams, String token) throws BtbException {
        LOGGER.debug("BaseUrl: " + baseURL);
        String completeRequest = baseURL + "/" + restFunctionWithParams;
        LOGGER.debug("CompleteUrl: " + completeRequest);
        // create HTTP Client
        CloseableHttpClient httpClient = createHttpsClient();

        // create new Request with given URL
        HttpGet getRequest = new HttpGet(completeRequest);
        getRequest.addHeader("accept", Globals.HEADER_JSON_UTF8);

        if (token != null)
        {
            String accesstoken = JwtHelper.formatToken(token);
            getRequest.addHeader(Globals.KEYCLOAK_AUTH_TAG, "Bearer " + accesstoken);
        } else {
            throw new BtbUnauthorized();
        }

        HttpResponse response;
        // Execute request an catch response
        try {
            response = httpClient.execute(getRequest);

        } catch (IOException e) {
            String errtext = "Communication to <" + completeRequest + "> failed!";
            LOGGER.warn(errtext, e);
            throw new BtbServiceUnavailable(errtext);
        }

        return createJson(response);
    }

	public String performPostRequest(String restFunctionWithParams, String token, String data) throws BtbException {
		String completeRequest = baseURL + "/" + restFunctionWithParams;

		// create HTTP Client
		CloseableHttpClient httpClient = createHttpsClient();

		// create new Post Request with given URL
		HttpPost postRequest = new HttpPost(completeRequest);

		// add additional header to getRequest which accepts application/JSON data
		postRequest.addHeader("accept", Globals.HEADER_JSON_UTF8);
		postRequest.addHeader("Content-Type", Globals.HEADER_JSON_UTF8);

		if (token != null) {
			String accesstoken = JwtHelper.formatToken(token);
			postRequest.addHeader(Globals.KEYCLOAK_AUTH_TAG, "Bearer " + accesstoken);
		} else {
			throw new BtbUnauthorized();
		}

		postRequest.setEntity(new StringEntity(data, StandardCharsets.UTF_8));

		HttpResponse response;
		// Execute request an catch response
		try {
			response = httpClient.execute(postRequest);
		} catch (IOException e) {
			String errtext = "Communication to <" + completeRequest + "> failed!";
			LOGGER.warn(errtext, e);
			throw new BtbServiceUnavailable(errtext);
		}
		return createJson(response);
	}

    private CloseableHttpClient createHttpsClient() throws BtbInternalServerError {
        if (useHttps) {
            try {
                SSLContextBuilder builder = new SSLContextBuilder();
                builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
                SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(builder.build());

                return HttpClients.custom().setSSLSocketFactory(sslsf).build();
            } catch (Exception e) {
                LOGGER.error(e.getMessage(),e);
                throw new BtbInternalServerError("SSLContextBuilderException");
            }
        } else {
            return HttpClientBuilder.create().build();
        }
    }
    
	private String createJson(HttpResponse response) throws BtbException {
		String retJson;
		try {
			retJson = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);
		} catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
			throw new BtbInternalServerError("IOException");
		}

		if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
			ErrorReturn errorReturn = new ErrorReturn();
			errorReturn.setErrorCode(response.getStatusLine().getStatusCode());
			errorReturn.setErrorText(response.getStatusLine().getReasonPhrase());
			throw new BtbNestedException(errorReturn);
		}
		return retJson;
	}

}
