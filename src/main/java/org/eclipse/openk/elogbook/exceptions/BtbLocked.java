/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.exceptions;

import org.apache.http.HttpStatus;

public class BtbLocked extends BtbException {
    public BtbLocked() {
        super();
    }

    public BtbLocked(String message) {
        super(message);
    }

    @Override
    public int getHttpStatus() {
        return HttpStatus.SC_LOCKED;
    }
}
