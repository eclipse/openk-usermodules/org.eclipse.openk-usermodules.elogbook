/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.exceptions;

import org.apache.http.HttpStatus;
import org.eclipse.openk.elogbook.common.JsonGeneratorBase;
import org.eclipse.openk.elogbook.viewmodel.ErrorReturn;
import org.eclipse.openk.elogbook.viewmodel.GeneralReturnItem;

public final class BtbExceptionMapper {
    private BtbExceptionMapper() {}

    public static String unknownErrorToJson() {
        ErrorReturn er = new ErrorReturn();
        er.setErrorText("Unknown Error");
        er.setErrorCode(HttpStatus.SC_INTERNAL_SERVER_ERROR);
        return JsonGeneratorBase.getGson().toJson(er);
    }

    public static String toJson(BtbException e) {
            ErrorReturn er = new ErrorReturn();
            er.setErrorText(e.getMessage());
        er.setErrorCode(e.getHttpStatus());
            return JsonGeneratorBase.getGson().toJson(er);
    }

    public static String getGeneralErrorJson() {
        return JsonGeneratorBase.getGson().toJson(new GeneralReturnItem("NOK"));
    }

    public static String getGeneralOKJson() {
        return JsonGeneratorBase.getGson().toJson(new GeneralReturnItem("OK"));
    }
}
