/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.viewmodel;

import java.util.Date;
import java.util.List;

/** View Model documenting all shift changes over a specified period. */
public class HistoricalShiftChanges {

	/** begin of the specified period */
	private Date transferDateFrom;

	/** end of the specified period */
	private Date transferDateTo;

	/** Collection of historical shift change documentation data. */
	private List<HistoricalResponsibility> historicalResponsibilities;

	public Date getTransferDateFrom() {
		return transferDateFrom;
	}

	public void setTransferDateFrom(Date startDate) {
		this.transferDateFrom = startDate;
	}

	public Date getTransferDateTo() {
		return transferDateTo;
	}

	public void setTransferDateTo(Date endDate) {
		this.transferDateTo = endDate;
	}

	public List<HistoricalResponsibility> getHistoricalResponsibilities() {
		return historicalResponsibilities;
	}

	public void setHistoricalResponsibilities(List<HistoricalResponsibility> historicalResponsibilities) {
		this.historicalResponsibilities = historicalResponsibilities;
	}

}
