/* ********************************************************************************** */
 -- Task: [OK-351/combined OK-353] create an index on columns create_date and mod_date
 --       as these columns are relevant for fast search.
/* ********************************************************************************** */

CREATE INDEX create_date_idx
  ON public.tbl_notification(create_date);
  
CREATE INDEX mod_date_idx
  ON public.tbl_notification(mod_date);
